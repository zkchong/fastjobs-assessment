import React from "react";

interface RandomListProps {
  length: number;
}

function RandomList({ length }: RandomListProps) {
  const numbers: number[] = [];

  for (let i = 0; i < length; i++) {
    // between 1 and 100
    const number = Math.floor(Math.random() * 100) + 1;
    numbers.push(number);
  }

  return (
    <ul>
      {numbers.map((number, index) => (
        <li key={index}>{number}</li>
      ))}
    </ul>
  );
}

export default RandomList;
