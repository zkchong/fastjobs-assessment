import React, { useEffect, useState } from "react";
import axios from "axios";

interface Item {
  id: number;
  name: string;
  quantity: number;
}

function ItemList() {
  const [items, setItems] = useState<Item[]>([]);

  useEffect(() => {
    axios
      .get<Item[]>("/items")
      .then((response) => {
        setItems(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  return (
    <ul>
      {items.map((item) => (
        <li key={item.id}>
          <h3>Name: {item.name}</h3>
          <p>Quantity: {item.quantity}</p>
        </li>
      ))}
    </ul>
  );
}

export default ItemList;
