import { Controller, Get, Query } from '@nestjs/common';
import { Post } from './posts.entity';
import { PostsService } from './posts.service';

@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}
  @Get()
  async searchPosts(@Query('q') query: string): Promise<{ posts: Post[] }> {
    const posts = await this.postsService.searchPosts(query);
    return {
      posts,
    };
  }
}
