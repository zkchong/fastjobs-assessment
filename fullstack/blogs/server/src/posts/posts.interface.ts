export interface IPosts {
  id: string;
  title: string;
  body: string;
  createdAt: Date;
  updatedAt: Date;
}
