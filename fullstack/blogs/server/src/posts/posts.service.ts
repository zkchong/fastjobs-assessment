import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';

import { Post } from './posts.entity';

@Injectable()
export class PostsService {
  constructor(
    @InjectRepository(Post)
    private postRepository: Repository<Post>,
  ) {}

  async searchPosts(query: string): Promise<Post[]> {
    const posts = await this.postRepository.find({
      where: {
        title: Like(`%${query}%`),
      },
    });
    return posts;
  }

  async searchPostsMoreEfficient(query: string): Promise<Post[]> {
    const posts = await this.postRepository
      .createQueryBuilder('post')
      .where(
        `to_tsvector('simple', post.title) @@ plainto_tsquery('simple', :query)`,
        { query },
      )
      .getMany();
    return posts;
  }
}
