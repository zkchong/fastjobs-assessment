import 'dotenv/config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import type { PostgresConnectionCredentialsOptions } from 'typeorm/driver/postgres/PostgresConnectionCredentialsOptions';

const DB_BASE_CONFIG: PostgresConnectionCredentialsOptions = {
  database: 'blogs',
  host: process.env.DB_HOST,
  port: 5432,
  password: process.env.DB_PASSWORD,
  username: process.env.DB_USERNAME,
};

export const DB_CONFIG: TypeOrmModuleOptions = {
  type: 'postgres',
  replication: {
    master: {
      ...DB_BASE_CONFIG,
    },
    slaves: [
      {
        ...DB_BASE_CONFIG,
      },
    ],
  },
  synchronize: false,
};
