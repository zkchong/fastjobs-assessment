import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PostsModule } from './posts/posts.module';
import { DB_CONFIG } from './common/config/db.config';

@Module({
  imports: [TypeOrmModule.forRoot(DB_CONFIG), PostsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
