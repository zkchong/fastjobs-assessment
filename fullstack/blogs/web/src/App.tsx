import { useState } from "react";
import { useSearchParams } from "react-router-dom";
import axios from "axios";

interface BlogPost {
  id: number;
  title: string;
  body: string;
  createdAt: string;
  updatedAt: string;
}

const api = axios.create({
  baseURL: "http://localhost:3000",
});

function App() {
  const [, setSearchParams] = useSearchParams({ q: "" });

  const [searchQuery, setSearchQuery] = useState("");
  const [posts, setPosts] = useState<BlogPost[]>([]);

  const handleSearch = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setSearchParams({ q: searchQuery });
    try {
      const response = await api.get<BlogPost[]>(
        `/post/?q=${encodeURIComponent(searchQuery)}`
      );
      setPosts(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="container mx-auto py-6">
      <form onSubmit={handleSearch} className="mb-4">
        <div className="flex">
          <input
            type="text"
            value={searchQuery}
            onChange={(event) => {
              setSearchQuery(event.target.value);
              setSearchParams({ q: event.target.value });
            }}
            placeholder="Search for blog posts..."
            className="w-full px-4 py-2 mr-2 border border-gray-400 rounded-lg shadow-sm focus:outline-none focus:ring-2 focus:ring-blue-400"
          />
          <button
            type="submit"
            className="px-4 py-2 font-semibold text-white bg-blue-500 rounded-lg shadow-md"
          >
            Search
          </button>
        </div>
      </form>
      {posts.length > 0 ? (
        <div className="grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-3">
          {posts.map((post) => (
            <div key={post.id} className="border border-gray-300 rounded-lg shadow-sm">
              <h2 className="px-4 py-2 text-lg font-medium">{post.title}</h2>
              <div className="px-4 py-2 text-sm text-gray-500">{post.createdAt}</div>
              <div className="px-4 py-2">{post.body}</div>
            </div>
          ))}
        </div>
      ) : (
        <div className="text-center text-gray-500">No matching posts found.</div>
      )}
    </div>
  );
}

export default App;
