import type { Request, Response } from "express";
import { z } from "zod";
import { isObjectIdOrHexString } from "mongoose";

import ScheduleModel from "../models/schedule.model";

const scheduleSchema = z.object({
  title: z.string().min(1).max(50).trim(),
  description: z.string().min(1).trim(),
  startDate: z.string().datetime(),
  endDate: z.string().datetime(),
});

export const createSchedule = async (req: Request, res: Response): Promise<void> => {
  try {
    const scheduleData = scheduleSchema.parse(req.body);
    const userId = req.userId;
    const schedule = new ScheduleModel({ ...scheduleData, userId });
    await schedule.save();
    res.status(201).send(schedule);
  } catch (err) {
    if (err instanceof z.ZodError) {
      res.status(400).send({ errors: err.errors });
      return;
    }
    res.status(400).send((err as Error)?.message);
  }
};

export const getSchedules = async (req: Request, res: Response): Promise<void> => {
  try {
    const userId = req.userId;
    const schedules = await ScheduleModel.find({
      userId,
    })
      .lean()
      .exec();
    res.send(schedules);
  } catch (err) {
    res.status(500).send((err as Error)?.message);
  }
};

export const getScheduleById = async (req: Request, res: Response): Promise<void> => {
  try {
    const id = req.params.id;
    const userId = req.userId;

    if (!isObjectIdOrHexString(id)) {
      res.status(400).json({
        error: "invalid id",
      });
      return;
    }

    const schedule = await ScheduleModel.findOne({
      id,
      userId,
    })
      .lean()
      .exec();
    if (!schedule) {
      throw new Error("Schedule not found");
    }
    res.send(schedule);
  } catch (err) {
    res.status(404).send((err as Error).message);
  }
};

const updateScheduleSchema = z
  .object({
    title: z.string().min(1).max(50).optional(),
    description: z.string().min(1).optional(),
    startDate: z.string().datetime().optional(),
    endDate: z.string().datetime().optional(),
  })
  .partial()
  .refine(
    (data) => data.title ?? data.description ?? data.startDate ?? data.endDate,
    "at least 1 field must be given"
  );

export const updateSchedule = async (req: Request, res: Response): Promise<void> => {
  try {
    const id = req.params.id;
    const userId = req.userId;
    const updateData = updateScheduleSchema.parse(req.body);
    const schedule = await ScheduleModel.findOneAndUpdate(
      {
        id,
        userId,
      },
      updateData,
      { new: true }
    );
    if (!schedule) {
      throw new Error("Schedule not found");
    }
    res.send(schedule);
  } catch (err) {
    res.status(404).send((err as Error).message);
  }
};

export const deleteSchedule = async (req: Request, res: Response): Promise<void> => {
  try {
    const id = req.params.id;
    const userId = req.userId;

    if (!isObjectIdOrHexString(id)) {
      res.status(400).json({
        error: "invalid id",
      });
      return;
    }

    const schedule = await ScheduleModel.findOneAndDelete({
      id,
      userId,
    });
    if (!schedule) {
      throw new Error("Schedule not found");
    }
    res.send(schedule);
  } catch (err) {
    res.status(404).send((err as Error).message);
  }
};
