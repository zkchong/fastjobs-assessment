import { Router } from "express";
import { authMiddleware } from "../middlewares/auth.middleware";
import {
  createSchedule,
  deleteSchedule,
  getScheduleById,
  getSchedules,
  updateSchedule,
} from "./schedule.controller";
import { login, register } from "./auth.controller";

const router = Router();

router.post("/login", login);

router.post("/register", register);

router.post("/schedules", authMiddleware, createSchedule);

router.get("/schedules", authMiddleware, getSchedules);

router.get("/schedules/:id", authMiddleware, getScheduleById);

router.put("/schedules/:id", authMiddleware, updateSchedule);

router.delete("/schedules/:id", authMiddleware, deleteSchedule);

export default router;
