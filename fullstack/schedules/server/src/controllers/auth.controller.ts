import { Request, Response } from "express";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import { z } from "zod";

import UserModel from "../models/user.model";
import { jwtConstants } from "../constants/jwt";

const registerSchema = z.object({
  email: z.string().min(1).email().trim(),
  password: z.string().min(8, { message: "Password must be at least 8 characters" }),
});

export const register = async (req: Request, res: Response) => {
  try {
    const registerData = registerSchema.parse(req.body);

    const { email, password } = registerData;
    const user = new UserModel({ email, password });
    const savedUser = await user.save();

    const token = jwt.sign({ userId: savedUser._id }, jwtConstants.secret, {
      algorithm: "HS256",
      expiresIn: jwtConstants.duration,
    });
    res.status(201).send({ token });
  } catch (err) {
    if (err instanceof z.ZodError) {
      return res.status(400).json({
        errors: err.errors,
      });
    }
    res.status(400).send((err as Error).message);
  }
};

const loginSchema = z.object({
  email: z.string().min(1).email(),
  password: z.string().min(8),
});

export const login = async (req: Request, res: Response) => {
  try {
    const loginData = loginSchema.parse(req.body);
    const { email, password } = loginData;
    const user = await UserModel.findOne({ email });
    if (!user) {
      throw new Error("Invalid email or password");
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      throw new Error("Invalid email or password");
    }
    const token = jwt.sign({ userId: user._id }, jwtConstants.secret, {
      algorithm: "HS256",
      expiresIn: jwtConstants.duration,
    });
    res.send({ token });
  } catch (err) {
    if (err instanceof z.ZodError) {
      return res.status(400).json({
        errors: err.errors,
      });
    }
    res.status(401).send((err as Error).message);
  }
};
