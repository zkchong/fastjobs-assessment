import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
import { jwtConstants } from "../constants/jwt";

interface AuthToken {
  userId: string;
}

export const authMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const token = req.header("Authorization")?.replace("Bearer ", "");

  if (!token) {
    return res.status(401).send({ error: "Unauthorized" });
  }

  try {
    const decoded = jwt.verify(token, jwtConstants.secret, {
      algorithms: ["HS256"],
      ignoreExpiration: false,
    });
    req.userId = (decoded as AuthToken).userId;
    next();
  } catch (err) {
    res.status(401).send({ error: "Unauthorized" });
  }
};
