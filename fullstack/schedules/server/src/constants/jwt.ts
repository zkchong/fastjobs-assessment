export const jwtConstants = Object.freeze({
  // should be an env, but for assessment sake we hardcode
  secret: "thisobviouslyneedstobestoredinenv",
  duration: "1h",
});
