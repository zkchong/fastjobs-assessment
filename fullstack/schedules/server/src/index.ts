import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import router from "./controllers";

const mongoURI =
  "mongodb+srv://admin:tmtkC61mRemNt8YR@fastjobs.uhk97af.mongodb.net/fastjobs?retryWrites=true&w=majority";

const app = express();
app.use(cors());
app.use(express.json());

app.get("/healthz", (req, res) => {
  res.send("ok");
});
app.use("/api/v1", router);

const port = process.env.PORT ?? 3000;

async function main(): Promise<void> {
  try {
    await mongoose.connect(mongoURI);
    app.listen(port, () => {
      console.log(`Server listening on port ${port}`);
    });
  } catch (err) {
    console.error(`Error: ${(err as Error).message}`);
    process.exit(1);
  }
}

main();
