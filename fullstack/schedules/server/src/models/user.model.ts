import mongoose, { Document } from "mongoose";
import bcrypt from "bcrypt";

interface User extends Document {
  email: string;
  password: string;
  comparePassword: (userPassword: string) => Promise<boolean>;
}

const UserSchema = new mongoose.Schema<User>(
  {
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
    },
    password: {
      type: String,
      required: true,
      trim: true,
    },
  },
  {
    toJSON: {
      virtuals: true,
    },
  }
);

UserSchema.pre<User>("save", async function (next) {
  const user = this;
  if (user.isModified("password") || user.isNew) {
    try {
      const salt = await bcrypt.genSalt(10);
      const hash = await bcrypt.hash(user.password, salt);
      user.password = hash;
      next();
    } catch (err) {
      return next(err as Error);
    }
  } else {
    return next();
  }
});

UserSchema.methods.comparePassword = async function (userPassword: string) {
  return bcrypt.compare(userPassword, this.password);
};

const UserModel = mongoose.model<User>("User", UserSchema);

export default UserModel;
