import mongoose, { Document } from "mongoose";

interface Schedule extends Document {
  userId: mongoose.Schema.Types.ObjectId;
  title: string;
  description: string;
  startDate: Date;
  endDate: Date;
}

const ScheduleSchema = new mongoose.Schema<Schedule>(
  {
    title: {
      type: String,
      required: true,
      trim: true,
    },
    startDate: {
      type: Date,
      required: true,
    },
    endDate: {
      type: Date,
      required: true,
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "User",
    },
  },
  {
    toJSON: {
      virtuals: true,
    },
  }
);

const ScheduleModel = mongoose.model<Schedule>("Schedule", ScheduleSchema);

export default ScheduleModel;
