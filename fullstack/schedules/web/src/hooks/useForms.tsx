import { useState, FormEvent, ChangeEvent } from "react";

type FormValues = Record<string, string | Date>;

interface UseFormsProps<T> {
  initialValues: T;
  onSubmit: (values: T) => void;
}

function useForms<T extends FormValues>({ initialValues, onSubmit }: UseFormsProps<T>) {
  const [values, setValues] = useState<T>(initialValues);

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setValues({ ...values, [name]: value } as T);
  };

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    onSubmit(values);
  };

  return {
    values,
    handleChange,
    handleSubmit,
  };
}

export default useForms;
