import { createBrowserRouter } from "react-router-dom";
import PrivateRoute from "./components/PrivateRouteGuard";
import LoginPage from "./pages/login";
import RegisterPage from "./pages/register";
import SchedulePage from "./pages/schedule";

export const routes = createBrowserRouter([
  {
    path: "/",
    element: <PrivateRoute />,
    children: [
      {
        path: "/",
        element: <SchedulePage />,
      },
    ],
  },
  {
    path: "/login",
    element: <LoginPage />,
  },
  {
    path: "/register",
    element: <RegisterPage />,
  },
]);
