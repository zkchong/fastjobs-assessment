import { useState } from "react";
import { Link } from "react-router-dom";
import { Schedule } from "../types";

interface ScheduleListProps {
  schedules: Schedule[];
  onEditSchedule: (schedule: Schedule) => void;
  onDeleteSchedule: (id: string) => void;
}

const ScheduleList = ({
  schedules,
  onEditSchedule,
  onDeleteSchedule,
}: ScheduleListProps) => {
  const [editedScheduleId, setEditedScheduleId] = useState<string>("");

  const handleEditSchedule = (schedule: Schedule) => {
    onEditSchedule(schedule);
    setEditedScheduleId(schedule.id);
  };

  const handleDeleteSchedule = (id: string) => {
    onDeleteSchedule(id);
    if (id === editedScheduleId) {
      setEditedScheduleId("");
    }
  };

  return (
    <div className="bg-white shadow-md rounded-md p-4">
      <h2 className="text-lg font-medium text-gray-700 mb-2">Schedules</h2>
      <div className="overflow-x-auto">
        <table className="table-auto w-full">
          <thead>
            <tr className="border-b border-gray-300">
              <th className="px-4 py-2 text-left text-sm font-medium text-gray-700">
                Title
              </th>
              <th className="px-4 py-2 text-left text-sm font-medium text-gray-700">
                Description
              </th>
              <th className="px-4 py-2 text-left text-sm font-medium text-gray-700">
                Start Date
              </th>
              <th className="px-4 py-2 text-left text-sm font-medium text-gray-700">
                End Date
              </th>
              <th className="px-4 py-2 text-right text-sm font-medium text-gray-700">
                Actions
              </th>
            </tr>
          </thead>
          <tbody>
            {schedules.map((schedule) => (
              <tr
                key={schedule.id}
                className={`border-b border-gray-300 ${
                  editedScheduleId === schedule.id ? "bg-gray-100" : ""
                }`}
              >
                <td className="px-4 py-2 text-sm font-medium text-gray-700">
                  {schedule.title}
                </td>
                <td className="px-4 py-2 text-sm font-medium text-gray-700">
                  {schedule.description}
                </td>
                <td className="px-4 py-2 text-sm font-medium text-gray-700">
                  {new Date(schedule.startDate).toLocaleDateString()}
                </td>
                <td className="px-4 py-2 text-sm font-medium text-gray-700">
                  {new Date(schedule.endDate).toLocaleDateString()}
                </td>
                <td className="px-4 py-2 text-right">
                  <Link
                    to="#"
                    onClick={(event) => {
                      event.preventDefault();
                      handleEditSchedule(schedule);
                    }}
                    className="text-indigo-600 hover:text-indigo-900 mr-2"
                  >
                    Edit
                  </Link>
                  <button
                    onClick={() => handleDeleteSchedule(schedule.id)}
                    className="text-red-600 hover:text-red-900"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default ScheduleList;
