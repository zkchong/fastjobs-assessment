import React from "react";
import useForms from "../hooks/useForms";
import type { Schedule } from "../types";

type NewSchedule = Omit<Schedule, "id">;

interface ScheduleFormProps {
  onCreateSchedule: (schedule: NewSchedule) => void;
}

const ScheduleForm: React.FC<ScheduleFormProps> = ({ onCreateSchedule }) => {
  const { values, handleChange, handleSubmit } = useForms({
    initialValues: {
      title: "",
      description: "",
      startDate: new Date(),
      endDate: new Date(),
    } satisfies NewSchedule,
    onSubmit: (formData) => {
      const startDate = new Date(formData.startDate);
      const endDate = new Date(formData.endDate);
      onCreateSchedule({
        ...formData,
        startDate,
        endDate,
      });
    },
  });

  return (
    <div className="bg-white shadow-md rounded-md p-4">
      <form onSubmit={handleSubmit}>
        <div className="mb-4">
          <label htmlFor="title" className="block text-sm font-medium text-gray-700 mb-1">
            Title
          </label>
          <input
            id="title"
            name="title"
            type="text"
            value={values.title}
            onChange={handleChange}
            required
            className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
            placeholder="Enter a title for your schedule"
          />
        </div>
        <div className="mb-4">
          <label
            htmlFor="description"
            className="block text-sm font-medium text-gray-700 mb-1"
          >
            Description
          </label>
          <input
            id="description"
            name="description"
            type="text"
            value={values.description}
            onChange={handleChange}
            required
            className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
            placeholder="Enter a description for your schedule"
          />
        </div>
        <div className="mb-4">
          <label
            htmlFor="startDate"
            className="block text-sm font-medium text-gray-700 mb-1"
          >
            Start Date
          </label>
          <input
            id="startDate"
            name="startDate"
            type="date"
            value={values.startDate.toLocaleString()}
            onChange={handleChange}
            required
            className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
          />
        </div>
        <div className="mb-4">
          <label
            htmlFor="endDate"
            className="block text-sm font-medium text-gray-700 mb-1"
          >
            End Date
          </label>
          <input
            id="endDate"
            name="endDate"
            type="date"
            value={values.endDate.toLocaleString()}
            onChange={handleChange}
            required
            className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
          />
        </div>
        <div className="text-right">
          <button
            type="submit"
            className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm
                      text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700
                      focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
          >
            Create schedule
          </button>
        </div>
      </form>
    </div>
  );
};

export default ScheduleForm;
