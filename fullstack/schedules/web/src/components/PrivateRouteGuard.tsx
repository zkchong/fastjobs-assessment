import { useEffect } from "react";
import { useNavigate, Outlet, type OutletProps } from "react-router-dom";

const PrivateRoute = (props: OutletProps): JSX.Element => {
  const navigate = useNavigate();

  useEffect(() => {
    const isAuthenticated = localStorage.getItem("token");

    if (!isAuthenticated) {
      navigate("/login");
    }
  }, []);

  return <Outlet {...props} />;
};

export default PrivateRoute;
