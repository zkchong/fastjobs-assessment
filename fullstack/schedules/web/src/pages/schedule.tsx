import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import jwtDecode from "jwt-decode";

import {
  getAllSchedules,
  deleteSchedule,
  updateSchedule,
  createSchedule,
} from "../api/apiClient";
import type { Schedule } from "../types";
import ScheduleList from "../components/ScheduleList";
import ScheduleForm from "../components/ScheduleForm";

export default function SchedulePage() {
  const navigate = useNavigate();

  const [schedules, setSchedules] = useState<Schedule[]>([]);

  useEffect(() => {
    const token = localStorage.getItem("token");

    if (!token || jwtDecode<{ exp: number }>(token).exp < Date.now() / 1000) {
      navigate("/login");
    }

    getAllSchedules()
      .then(({ data }) => {
        setSchedules(data);
      })
      .catch((err) => {
        if (err.status === 401) {
          navigate("/login");
          return;
        }

        console.error(err);
      });
  }, []);

  const onEditSchedule = (updatedSchedule: Schedule) =>
    updateSchedule(updatedSchedule.id, {
      title: updatedSchedule.title,
      description: updatedSchedule.description,
      endDate: updatedSchedule.endDate,
      startDate: updatedSchedule.startDate,
    });

  const onDeleteSchedule = (id: string) => deleteSchedule(id);

  const onCreateSchedule = (schedule: Omit<Schedule, "id">) => {
    createSchedule(schedule);
  };

  return (
    <>
      <ScheduleForm onCreateSchedule={onCreateSchedule} />
      <ScheduleList
        schedules={schedules}
        onDeleteSchedule={onDeleteSchedule}
        onEditSchedule={onEditSchedule}
      />
    </>
  );
}
