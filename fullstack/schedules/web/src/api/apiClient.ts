import axios from "axios";

const apiClient = axios.create({
  baseURL: "http://localhost:3000/api/v1",
});

export const login = async ({ email, password }: { email: string; password: string }) => {
  return apiClient.post("/login", { email, password });
};

export const register = async ({
  email,
  password,
}: {
  email: string;
  password: string;
}) => {
  return apiClient.post("/register", { email, password });
};

const getToken = () => localStorage.getItem("token");

interface BaseSchedule {
  title: string;
  description: string;
  startDate: Date;
  endDate: Date;
}
export const createSchedule = async (schedule: BaseSchedule) =>
  apiClient.post("/schedules", schedule, {
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  });

export const getAllSchedules = async () =>
  apiClient.get("/schedules", {
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  });

export const getSchedule = async (id: string) =>
  apiClient.get(`/schedule/${id}`, {
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  });

export const updateSchedule = async (
  id: string,
  scheduleUpdates: Partial<BaseSchedule>
) =>
  apiClient.put(`/schedule/${id}`, scheduleUpdates, {
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  });

export const deleteSchedule = async (id: string) =>
  apiClient.delete(`/schedule/${id}`, {
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  });
