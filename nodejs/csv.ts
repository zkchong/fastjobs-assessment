/**
 *
 * This assumes that the CSV contains users information
 * that needs to be inserted into the mongodb users collection
 */
import mongoose from "mongoose";
import * as Papa from "papaparse";
import * as fs from "fs";

const mongoURI = "mongodb://localhost:27017/mydatabase";

interface Users {
  name: string;
  age: number;
  email: string;
}

const UsersSchema = new mongoose.Schema<Users>({
  name: String,
  age: Number,
  email: String,
});

const usersModel = mongoose.model("users", UsersSchema);

async function parseCSV(file: string) {
  return new Promise<Users[]>((resolve, reject) => {
    const csvData = fs.readFileSync(file, "utf8");

    return Papa.parse<Users>(csvData, {
      header: true,
      complete: (results) => {
        resolve(results.data);
      },
      error: (err) => {
        reject(err);
      },
    });
  });
}

async function connectToMongoDB() {
  return mongoose.connect(mongoURI);
}

async function insertData(csvData: Users[]): Promise<void> {
  await usersModel.insertMany(csvData);
}

async function main(): Promise<void> {
  try {
    await connectToMongoDB();
    const file = process.argv[2];
    const data = await parseCSV(file);
    await insertData(data);
    console.log(`Data inserted successfully into MongoDB!`);
    process.exit(0);
  } catch (err) {
    console.error(`Error: ${(err as Error).message}`);
    process.exit(1);
  }
}

// ts-node csv.ts <filename>.csv
main();
