import type { Request, Response } from "express";
import UserModel from "../models/user.model";

export async function createUser(req: Request, res: Response) {
  try {
    const user = new UserModel(req.body);
    await user.save();
    res.status(201).send(user);
  } catch (err) {
    res.status(400).send(err.message);
  }
}

export async function deleteUser(req: Request, res: Response) {
  try {
    const id = req.params.id;
    const user = await UserModel.findByIdAndDelete(id);
    if (!user) {
      throw new Error("User not found");
    }
    res.send(user);
  } catch (err) {
    res.status(404).send(err.message);
  }
}
