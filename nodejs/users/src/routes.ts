import { Router } from "express";
import { createUser, deleteUser } from "./controllers/user.controller";

const router = Router();

router.post("/users", createUser);
router.delete("/users/:id", deleteUser);

export default router;
