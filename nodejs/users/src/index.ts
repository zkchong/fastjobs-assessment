import express from "express";
import mongoose from "mongoose";

const app = express();

const mongoURI = "mongodb://localhost:27017/mydatabase";

async function connectToMongoDB() {
  return mongoose.connect(mongoURI);
}

async function main() {
  const port = 3000;
  try {
    await connectToMongoDB();
    app.listen(port, () => {
      console.log(`Server listening on port ${port}`);
    });
  } catch (err) {
    console.error(`Error: ${(err as Error).message}`);
    process.exit(1);
  }
}

main();
