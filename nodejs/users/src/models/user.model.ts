import mongoose, { Document } from "mongoose";
import bcrypt from "bcrypt";

export interface User extends Document {
  name: string;
  email: string;
  password: string;
}

const UserSchema = new mongoose.Schema<User>({
  name: String,
  email: String,
  password: String,
});

UserSchema.pre<User>("save", async function (next) {
  try {
    const user = this;
    if (!user.isModified("password")) {
      return next();
    }
    const hashedPassword = await bcrypt.hash(user.password, 10);
    user.password = hashedPassword;
    next();
  } catch (err) {
    next(err);
  }
});

const UserModel = mongoose.model<User>("User", UserSchema);

export default UserModel;
