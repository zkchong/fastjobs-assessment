# MERN Full-Stack Questionnaire

Answer any 2 questions from each part.

## Part 1: MongoDB

- Write a MongoDB query to find all documents in a collection where the "age" field is greater than 25. ✅
- Write a MongoDB query to update the "email" field of a document with a specific ID.
- Write a MongoDB query to delete all documents in a collection where the "status" field is set to "inactive". ✅

## Part 2: Express

- Create an Express route that returns a list of all users in a MongoDB collection as a JSON response. ✅
- Write an Express middleware function that logs the IP address and timestamp of each incoming request. ✅
- Create an Express route that allows a user to upload a file to the server and save it to a specified directory.

## Part 3: React

- Create a React component that displays a list of items retrieved from an API endpoint. ✅
- Write a React function that takes a number as input and returns a list of that length, with each item consisting of a random number between 1 and 100. ✅
- Create a React component that is a widget which is like a contact form (Name, Email, Phone)

## Part 4: Node.js

- Write a Node.js script that reads a CSV file and inserts the data into a MongoDB collection. ✅
- Create a Node.js server that allows users to create and delete user accounts, with the account information stored in a MongoDB collection. ✅
- Write a Node.js function that takes a string as input and returns the number of words in the string.

## Part 5: Full-Stack Application

- Create a full-stack MERN application that allows users to create, edit, and delete schedules, with the post data stored in a MongoDB collection. ✅
- Write a function that validates user input for a MERN stack application and returns an error message if the input is invalid.
- Create a feature that allows users to search for blog posts by keyword, with the search functionality implemented on both the client and server sides. ✅
