import type { Request, Response, NextFunction } from "express";

function requestLoggerMw(req: Request, res: Response, next: NextFunction) {
  const timestamp = new Date().toISOString();
  const ip = req.headers["x-forwarded-for"] ?? req.headers["x-real-ip"] ?? req.ip;
  console.log(`[${timestamp}] ${ip}`);
  next();
}

export default requestLoggerMw;
