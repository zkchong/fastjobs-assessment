import express, { NextFunction, Request, Response } from "express";
import mongoose from "mongoose";

interface User {
  name: string;
  email: string;
  password: string;
}

const userSchema = new mongoose.Schema<User>({
  name: String,
  email: String,
  password: String,
});

const User = mongoose.model<User>("User", userSchema);

async function connectToDatabase(uri: string, dbName: string) {
  await mongoose.connect(uri, {
    dbName,
  });
}

const app = express();

app.get("/users", async (req: Request, res: Response, next: NextFunction) => {
  try {
    const users = await User.find();
    return res.status(200).json(users);
  } catch (err) {
    next(err);
  }
});

async function main() {
  const dbUri = "mongodb://localhost:27017";
  const dbName = "exampleDb";
  await connectToDatabase(dbUri, dbName);

  app.listen(3000, () => {
    console.log("Server listening on port 3000");
  });
}

main();
